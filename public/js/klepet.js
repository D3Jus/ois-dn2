var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, geslo) {
    this.socket.emit('pridruzitevZahteva', {
      novKanal: kanal,
      geslo: geslo
    });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var ukazZasebno = ukaz;
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      var zasebni = kanal.match(/"(.*?)"/g);
      if(zasebni && zasebni.length >= 2) {
        this.spremeniKanal(zasebni[0].replace(/"/g, ''), zasebni[1].replace(/"/g, ''));
      } else {
        this.spremeniKanal(kanal);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      ukazZasebno = ukazZasebno.match(/"(.*?)"/g);
      ukazZasebno[0] = ukazZasebno[0].replace(/"/g, "");
      ukazZasebno[1] = ukazZasebno[1].replace(/"/g, "");
      
      this.socket.emit('zasebnoZahteva', {prejemnik: ukazZasebno[0], besedilo: filtrirajBesede(vstaviSmeske(ukazZasebno[1]))});
      $('#sporocila').append($('<div style="font-weight: bold"></div>').html('(zasebno za ' + ukazZasebno[0] + '): ' + filtrirajBesede(vstaviSmeske(ukazZasebno[1]))));
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};