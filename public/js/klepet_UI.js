var trenutniVzdevek;
var trenutniKanal;

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function vstaviSmeske(sporocilo) {
  var novoSporocilo = sporocilo.replace(/</g, '&lt;');
  novoSporocilo = novoSporocilo.replace(/>/g, '&gt;'); 
  novoSporocilo = novoSporocilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png" alt=";)" />');
  novoSporocilo = novoSporocilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png" alt=":)" />');
  novoSporocilo = novoSporocilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png" alt="(y)" />');
  novoSporocilo = novoSporocilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" alt=":*" />');
  novoSporocilo = novoSporocilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png" alt=":(" />');
  
  return novoSporocilo;
}

function filtrirajBesede(sporocilo) {
  for(var i=0; i < vulgarneBesede.length; i++) {
    sporocilo = sporocilo.replace(new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi'), function() { 
      var nadomestilo = '';
      for(var j=0; j < vulgarneBesede[i].length; j++) {
        nadomestilo += '*';
      } 
      return nadomestilo;
    });
  }
  
  return sporocilo;
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = filtrirajBesede(vstaviSmeske(sporocilo));
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();
var trenutniVzdevek;

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + ' @ ' + trenutniKanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + ' @ ' + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('zasebno', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  socket.on('zasebnoNapaka', function (podatki) {
    $('#sporocila').append(divElementHtmlTekst('Sporočila ' + podatki.besedilo + ' uporabniku z vzdevkom ' + podatki.prejemnik + ' ni bilo mogoče posredovati.'));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    var dolzinaSeznama = uporabniki.vzdevki.length;
    
    for(var i=0; i < dolzinaSeznama; i++) {
      $('#seznam-uporabnikov').append($('<div>').text(uporabniki.vzdevki[i]));
    }
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});